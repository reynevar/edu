#+TITLE: Gathered interview questions for dev position
#+AUTHOR: Lukasz Kowalczyk
* General topics
** SOLID
   S - Single responsibility ( one logical reason to change the class / class is responsible for one logical thing )
   O - Open/Closed principle (class is closed to modifications but opened to extention )
   L - Liskov Substitution Principle ( when substituting superclass with subclass (Subclass : Superclass ), behavior of defined methods (object) should be consistent )
   I - Interface segragation ( many small interfaces > one big interface )
   D - Dependency inversion ( high and low lvl modules should depend on abstractions, not on each other)
** Design patterns
*** Abstract factory
    creational; creates instance of objects based on arguments/defines. uses unified interface for factory objects and for objects itself.
*** Facade
    structural; provides interface for a group of subsystems, allowing easier usage for probably complex subsustems interfaces.
*** Adapter
    structural; translates different interfaces to allow communication between them / math interfaces from different classes
*** Observer
    behavioral; Notification mechanism used for sending events for subscribed object to inform them about some change.
*** TODO Command
    behavioral;
** vtable / inheritance
** memory management / RAII
** sorting algorithms
*** merge sort
*** quick sort
*** radix sort
** graphs ( bfs , dfs ) / suffix tree
** c++17 features with practice
** lambdas
   [capture list](argument list CONST){code};
** template class / method
#+begin_src C++
template<typename T>
T modifyNumber(T val) {return val*2-1;}
#+end_src
* Knowit
** threading, mutex
** make_unique
   Added in c++14
   - does not require redundant type usage ( unique_ptr<T>(new T()) -> make_unique<T>() )
   - adds additional exception safety over using std::unique_ptr or std::shared_ptr constructors.
   A reason to use std::unique_ptr(new A()) or std::shared_ptr(new A()) directly instead of std::make_*() is being unable to access the constructor of class A outside of current scope.(i.e.: private constructor + friend class )
** remove + erase for containers
   /std::remove/ checks for predicate and modifies container by shifting all elements that are not to be removed closer to the begginig of container.
   /std::remove/ returns past the last "valid" element iterator, which is taken by /std::erase/ to actually remove the element.
** passing function as argument
   #+BEGIN_SRC
   int add(int x, int y){return x+y;}
   int sub(int x, int y){return x-y;}
   int operation2(int x, int y,std::function<int(int, int)> function){return function(x,y);}
   std::cout <<"Values 1 & 3. std::function: Add:"<<operation2(1,3,&add)<<" Sub:"<<operation2(1,3,&sub) << std::endl;
   #+END_SRC
** Lambda
*** Basic lambda
   #+BEGIN_SRC C++
   [](){}; //yes, it compiles
    #+END_SRC
   [] - capture list, can have explicitly added objects (by value 'x' or by reference'&y') or deduced depending on usage in lambda ([&] - by reference , [=] - by value )
   () - argument list, by default it passed as /const/ ( have to add /mutable/ after brackets to allow modifications )
   {} - code
*** IIFE - Immediately Invoked Function Expression
    #+BEGIN_SRC
    int x = 1, y = 1;
    [&]() { ++x; ++y; }(); // <-- call lambda(), increase x and y values;
    #+END_SRC
*** C++14 changes
    - Captures With an Initializer
      auto foo = [z = x+y]() { std::cout << z << '\n'; }; // create new member to use inside lambda
    - Move operations
      #+BEGIN_SRC
      std::unique_ptr<int> p(new int{10});
      auto bar = [ptr=std::move(p)] {}; // move an object into a member
      #+END_SRC
    - Optimisations
      #+BEGIN_SRC
      std::vector<std::string> vs;
      std::find_if(vs.begin(), vs.end(), [](std::string const& s) {
      return s == "foo"s + "bar"s; });
      std::find_if(vs.begin(), vs.end(), [p="foo"s + "bar"s](std::string const& s) { return s == p; }); // Rather than computing some value every time we invoke a lambda, we can compute it once in the initialiser
      #+END_SRC
    - Generic Lambdas
      #+BEGIN_SRC
      auto foo = [](auto x) { std::cout << x << '\n'; };
      #+END_SRC
  - any behavioral pattern
    Command / Observer / Mediator
** SOLID
   describe one principle
** gtest
    - test P, F
    - difference between EXPECT_CALL vs ON_CALL
      /EXPECT_CALL/ sets expectation on a mock calls. Writing:
      #+BEGIN_SRC
      EXPECT_CALL(mock, methodX(_)).WillRepeatedly(do_action);
      #+END_SRC

      tells gMock that methodX may be called on mock any number of times with any arguments, and when it is, mock will perform do_action. On the other hand,
      #+BEGIN_SRC
      ON_CALL(mock, methodX(_)).WillByDefault(do_action);
      #+END_SRC
      tells gMock that whenever methodX is invoked on mock, it should perform do_action. That feature is helpful in a scenario where you have to write many expectations on your mock, and most/all of them have to specify the same action -- especially if it's complex.
      You can specify that action in /ON_CALL/, and then write /EXPECT_CALLs/ without specifying the action explicitly. E.g.,
      #+BEGIN_SRC
      ON_CALL(mock, Sign(Eq(0), _))
        .WillByDefault(DoAll(SetArgPointee<1>("argument is zero"), Return(0)));
      ON_CALL(mock, Sign(Gt(0), _))
        .WillByDefault(DoAll(SetArgPointee<1>("argument is positive"), Return(1)));
      ON_CALL(mock, Sign(Lt(0), _))
        .WillByDefault(DoAll(SetArgPointee<1>("argument is negative"), Return(-1)));
      #+END_SRC

      Now, if you have to write a lot of /EXPECT_CALLs/, you don't have to mock's specify the behavior every time:
      #+BEGIN_SRC
      EXPECT_CALL(mock, Sign(-4, _));
      EXPECT_CALL(mock, Sign(0, _));
      EXPECT_CALL(mock, Sign(1, _)).Times(2);
      EXPECT_CALL(mock, Sign(2, _));
      EXPECT_CALL(mock, Sign(3, _));
      EXPECT_CALL(mock, Sign(5, _));
      #+END_SRC

      In another example, assuming Sign returns int, if you write
      #+BEGIN_SRC
      ON_CALL(mock, Sign(Gt(0), _)).WillByDefault(Return(1));
      EXPECT_CALL(mock, Sign(10, _));
      #+END_SRC
      the call mock.Sign(10) will return 1 as /ON_CALL/ provides default behavior for a call specified by EXPECT_CALL. But if you write
      #+BEGIN_SRC
      EXPECT_CALL(mock, Sign(Gt(0), _).WillRepeatedly(Return(1));
      EXPECT_CALL(mock, Sign(10, _));
      #+END_SRC
      the invocation of mock.Sign(10, p) will return 0. It will be matched against the second expectation. That expectation specifies no explicit action and gMock will generate a default action for it.
      That default action is to return a default value of the return type, which is 0 for int. The first expectation will be totally ignored in this case.
  - ASSERT_ vs EXPECT_
    /EXPECT_/ is a non-fatal check ( will not terminate test case ) , /ASSERT_/ will stop current test in case of failure.
  - jenkins
* TTpsc
** C++17 known features
*** std::any / std::variant
*** std::byte
*** invoke
** how to ensure save communication between client and servers on sockets (i.e.: connection to bank account site)
*** TLS (Transport Layer Security )
    Encryption protocol used for secure internet connections. SSL (Secure Sockets Layer) was predacessor. 
*** encrypted communication vis tls process:
    1. TCP handshake between client and server
    2. client sends to sever list of cyphering algorithms that is can use
    3. server chooses one of them ( if there is a match ) and sends its certificate with chosen algorithm to client
    4. client verifies certificate
    5. client sends generated symmetric key encrypted using chosen algorithm to server (key is used only for *one* session)
*** communicatino with Deffie-Helman
    TODO
** asymetric key - RSA (Rivest–Shamir–Adleman), ECDH
   system for secure data transmission. Creates public + private key. Data cyphered with one of the keys cannot be decyphered with the same key. Public key is given to "clients", private stays on the server.
   RSA-2048 : uses 2048 bits to create big number (that was created of two prime numbers).
   Relatively slow mechanism.
** symmetric key - AES (Advanced Encryption Standard)
   symmetric key mechanism is used to encrypt and decrypt data. Divides data into "chunks" of 128/256/512? bits data and cypher them using the key and chosen implementation.
   possible implementations:
   - ECB (Electronic Codebook)
     First implementation for AES. The message is divided into blocks, and each block is encrypted separately.
   - CBC (Cipher Blocker Chaining)
     Each block of plaintext is XORed with the previous ciphertext block before being encrypted. This way, each ciphertext block depends on all plaintext blocks processed up to that point.
     To make each message unique, an initialization vector must be used in the first block.
     Cannot be paralellized, because prev encrypted blocks are used to encrypt next one.
   - CTR
** coding
*** return first 2 unique digits in string
#+begin_src c++
#include <string>
#include <iostream>
#include <vector>
#include <array>
#include <utility>
#include <unordered_set>
#include <algorithm>

template<typename T>
void printVec(T vec) {
    for(auto&v : vec) {
        std::cout << v << " ";
    }
    std::cout <<"\n";
}

std::vector<int> findDigits(const std::string& str) {
  std::array digits{0,0,0,0,0,0,0,0,0,0};
  std::unordered_set<int> digiSet;
  for (auto& i : str) {
      auto val = i-'0';
      digits[val]++;
      digiSet.insert(val);
  }
  std::vector<int> res;
  for(auto& s : digiSet) {
      if (digits[s] == 1) {
          res.emplace_back(s);
      }
  }
  std::reverse(res.begin(), res.end());
  if(res.empty())
    res.emplace_back(-1);
  else if (res.size() > 2)
    res = std::vector<int>{res.begin(), res.begin()+2};
  return res;
}

auto main() -> int {
    std::string s1{"111255578839"}; //2,7
    printVec(findDigits(s1));
    std::string s2{"12312351236"};  //5,6
    printVec(findDigits(s2));
    std::string s3{"011200110"};    //2
    printVec(findDigits(s3));
    return 0;
}
#+end_src
*** combine two u8 values as u16 using bits once from first once from second value
#+begin_src c++
uint8_t b1{0xFF};
uint8_t b2 = 0;

uint16_t ba{0};

uint8_t bitmask{0b10000000};
for (auto var = 15, i = 0; i < 8; ++i)
{
    auto bit1 = b1 & bitmask>>7-i;
    auto bit2 = b2 & bitmask>>7-i;
    bitmask >>= 1;
    ba |= bit1 << var--;
    ba |= bit2 << var--;
}
#+end_src
*** function takes 2 vector<int> and return vector containing all values except 4 highest ones. printout 4 left values as sum
#+begin_src C++
std::vector<int> combine(std::vector<int>& v1, std::vector<int>& v2) {
    std::vector<int> res = v1;
    res.insert(res.end(), v2.begin(), v2.end());
    std::sort(res.begin(),res.end());
    if(res.size() < 4)
        return {};
    std::cout << "SUM: " << std::accumulate(res.end()-4, res.end(), 0) <<std::endl;

    res.erase(res.end()-4, res.end());
    return res;
}

auto main() -> int {
    std::vector<int> v1{-1,20,10,0,20,21};
    std::vector<int> v2{5,-1,-1,30};
    printVec(combine(v1,v2));
    return 0;
}
#+end_src
*** codility
#+begin_src C++
#include <vector>
#include <sstream>
#include <stack>

std::vector<std::string> split(const std::string& s, char delim = ' ') {
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> elems;
  while (std::getline(ss, item, delim)) {
    elems.emplace_back(item);
  }
  return elems;
}

void print(std::vector<string>& str){
    for(auto& s : str)
        std::cout << s << " ";
    std::cout << "\n";
}

bool isInRange(int val) {
    static int max_uint20 = 0xFFFFF;
    if(max_uint20 >= val && val >= 0)
        return true;
    return false;
}

bool tryCombine(stack<int>& st, bool isAdd=false){
    bool res = false;
    if(st.size() >= 2) {
        auto v1 = st.top();
        st.pop();
        auto v2 = st.top();
        st.pop();
        int v3;
        if (isAdd)
            v3 = v1+v2;
        else
            v3 = v1-v2;
        if(isInRange(v3)) {
            st.push(v3);
            res = true;
        }
    }
    return res;
}

bool performOperation(const string& str, stack<int>& st) {
    bool res = false;
    switch(str[0]) {
        case 'D':
        {
            if (!st.empty())
                res = true;
                st.push(st.top());
            break;
        }
        case 'P':
        {
            if (!st.empty())
                res = true;
                st.pop();
            break;
        }
        case '+':
        {
            res = tryCombine(st , true);
            break;
        }
        case '-':
        {
            res = tryCombine(st);
            break;
        }
        default:
        {
            auto val = std::stoi(str);
            if (isInRange(val)) {
                st.push(val);
                res = true;
            }
        }
    }
    return res;
}

int solution(string &S) {
    auto sstr = split(S);
    // WARN: depending on the system, int max could be lower then 2^20-1, assuming it is ok, since function signature returns int
    std::stack<int> st;
    
    for (const auto& oper : sstr) {
        if (!performOperation(oper, st))
            return -1;
    }

    if (st.empty())
        return -1;
    return st.top();
}
#+end_src
